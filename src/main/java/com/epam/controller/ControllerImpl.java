package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

import java.io.File;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void jacksonParseList(File json) {
        model.jacksonParseList(json);
    }

    @Override
    public void gsonParseList(File json) {
        model.gsonParseList(json);
    }

    @Override
    public boolean validate(File json, File schema) {
        return model.validate(json, schema);
    }
}