package com.epam.controller;

import java.io.File;

public interface Controller {
    void jacksonParseList(File json);

    void gsonParseList(File json);

    boolean validate(File json, File schema);
}
