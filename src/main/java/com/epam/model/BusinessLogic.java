package com.epam.model;

import com.epam.json.JsonValidator;
import com.epam.json.gson.GsonParser;
import com.epam.json.jackson.JacksonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class BusinessLogic implements Model {
    private static Logger logger = LogManager.getLogger(BusinessLogic.class);
    private JacksonParser jacksonParser;
    private GsonParser gsonParser;

    @Override
    public void jacksonParseList(File json) {
        jacksonParser = new JacksonParser();
        logger.info("Jackson parser result: ");
        for (Plane plane : jacksonParser.parseList(json)) {
            logger.info(plane.toString());
        }
    }

    @Override
    public void gsonParseList(File json) {
        gsonParser = new GsonParser();
        logger.info("Gson parser result: ");
        for (Plane plane : gsonParser.parseList(json)) {
            logger.info(plane.toString());
        }
    }

    @Override
    public boolean validate(File json, File schema) {
        JsonValidator validator = new JsonValidator();
        return validator.isValid(json, schema);
    }
}
