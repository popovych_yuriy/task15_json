package com.epam.model;

import java.util.Objects;

public class Plane {
    private int planeId;
    private String model;
    private String country;
    private String role;
    private int crew;
    private int rockets;
    private boolean radar;
    private double length;
    private double wingspan;
    private double height;
    private int price;

    public Plane() {

    }

    public Plane(int planeId, String model, String country, String role, int crew, int rockets, boolean radar, double length, double wingspan, double height, int price) {
        this.planeId = planeId;
        this.model = model;
        this.country = country;
        this.role = role;
        this.crew = crew;
        this.rockets = rockets;
        this.radar = radar;
        this.length = length;
        this.wingspan = wingspan;
        this.height = height;
        this.price = price;
    }

    public int getPlaneId() {
        return planeId;
    }

    public void setPlaneId(int planeId) {
        this.planeId = planeId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getCrew() {
        return crew;
    }

    public void setCrew(int crew) {
        this.crew = crew;
    }

    public int getRockets() {
        return rockets;
    }

    public void setRockets(int rockets) {
        this.rockets = rockets;
    }

    public boolean isRadar() {
        return radar;
    }

    public void setRadar(boolean radar) {
        this.radar = radar;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWingspan() {
        return wingspan;
    }

    public void setWingspan(double wingspan) {
        this.wingspan = wingspan;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plane plane = (Plane) o;
        return planeId == plane.planeId &&
                crew == plane.crew &&
                rockets == plane.rockets &&
                radar == plane.radar &&
                length == plane.length &&
                wingspan == plane.wingspan &&
                height == plane.height &&
                price == plane.price &&
                model.equals(plane.model) &&
                Objects.equals(country, plane.country) &&
                Objects.equals(role, plane.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(planeId, model, country, role, crew, rockets, radar, length, wingspan, height, price);
    }

    @Override
    public String toString() {
        return "Plane{" +
                "planeId=" + planeId +
                ", model='" + model + '\'' +
                ", country='" + country + '\'' +
                ", role='" + role + '\'' +
                ", crew=" + crew +
                ", rockets=" + rockets +
                ", radar=" + radar +
                ", length=" + length +
                ", wingspan=" + wingspan +
                ", height=" + height +
                ", price=" + price +
                '}';
    }
}
