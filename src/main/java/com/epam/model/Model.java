package com.epam.model;

import java.io.File;

public interface Model {
    void jacksonParseList(File json);

    void gsonParseList(File json);

    boolean validate(File json, File schema);
}
