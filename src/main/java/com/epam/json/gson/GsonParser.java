package com.epam.json.gson;

import com.epam.model.Plane;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GsonParser {
    private JsonReader reader;
    private List<Plane> planes = new ArrayList<>();
    private Gson gson = new Gson();

    public List<Plane> parseList(File json) {
        try {
            reader = new JsonReader(new FileReader(json));
            planes = Arrays.asList(gson.fromJson(reader, Plane[].class));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return planes;
    }
}
