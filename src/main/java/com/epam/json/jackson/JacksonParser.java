package com.epam.json.jackson;

import com.epam.model.Plane;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {
    private ObjectMapper objectMapper = new ObjectMapper();
    private List<Plane> planes = new ArrayList<>();

    public List<Plane> parseList(File json) {
        try {
            planes = Arrays.asList(objectMapper.readValue(json, Plane[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return planes;
    }
}
