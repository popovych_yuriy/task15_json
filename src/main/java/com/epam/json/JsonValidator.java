package com.epam.json;

import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JsonValidator {
    private Schema schema;

    public boolean isValid(File jsonFile, File schemaFile) {
        try {
            JSONObject jsonSchema = new JSONObject(new JSONTokener(new FileReader(schemaFile)));
            JSONArray jsonArray = new JSONArray(new JSONTokener(new FileReader(jsonFile)));
            schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonArray);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }
}
