package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private File json = new File("src/main/resources/json/planes.json");
    private File schema = new File("src/main/resources/json/planesSchema.json");

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Jackson parse json to list");
        menu.put("2", "  2 - Gson parse json to list");
        menu.put("3", "  3 - Check json using schema");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::jacksonParse);
        methodsMenu.put("2", this::gsonParse);
        methodsMenu.put("3", this::validate);
    }

    private void validate() {
        if (controller.validate(json, schema)) {
            logger.info("Json is valid.");
        } else {
            logger.info("Json is not valid.");
        }
    }

    private void gsonParse() {
        controller.gsonParseList(json);
    }

    private void jacksonParse() {
        controller.jacksonParseList(json);
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
